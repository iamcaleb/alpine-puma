# alpine-passenger
Base puma image for Ruby on Rails and NodeJS apps running on nginx

# registry

`FROM registry.gitlab.com/iamcaleb/alpine-puma`

# uses

- `alpine: latest`
- `nginx: 1.14.0`
- `ruby: 2.5.1`
