FROM alpine
LABEL maintainer="iamcaleberic@tuta.io"
LABEL version="0.15"
ENV HOME="/home/app"

RUN PACKAGES="nginx ca-certificates ruby procps curl pcre libstdc++ libexecinfo ruby-bundler bash ruby-dev ruby-etc ruby-rake mariadb-dev ruby-rdoc libpng ruby-irb nodejs yarn ruby-bigdecimal" && \
    GEM_PACKAGES="build-base ruby-rdoc libpng-dev git libffi-dev sqlite-dev pngquant autoconf automake" && \
    BUILD_PACKAGES="linux-headers curl-dev pcre-dev libexecinfo-dev zlib" && \
    # create app user
    adduser -h /home/app -G root -D app && \
    apk update && apk add --update $PACKAGES $BUILD_PACKAGES $GEM_PACKAGES && \
    # install puma
    gem install puma
  # Add nginx config
WORKDIR /etc/nginx/conf/
RUN mkdir sites-available sites-enabled

COPY lib/nginx.conf nginx.conf

WORKDIR /home/app
EXPOSE 80 28080

CMD ["sh", "-c","puma -b unix:///var/run/puma.sock"]
